export const CATEGORIES ={
    'about' : 'About',
    'blog' : 'Blog',
    'contacts' : 'Contacts',
    'home' : 'Home',
    'more' : 'More',
    'our_products': 'Our Products'
}