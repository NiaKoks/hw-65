import React, { Component } from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import Home from "./Components/Home";
import Add from "./Components/Add";
import Edit from "./Components/Edit";
import './App.css';

class App extends Component {
  render() {
    return (
        <BrowserRouter>
          <Switch>
            <Route path="/" exact component={Home}></Route>
            <Route path="/add" component={Add}></Route>
            <Route path="/:id/edit" component={Edit}></Route>
          </Switch>
        </BrowserRouter>
    );
  }
}

export default App;
