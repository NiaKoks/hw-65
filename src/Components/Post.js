import React, {Component} from 'react';
import {CATEGORIES} from "../Categories";
// import Nav from "../Components/Navigation/Navigation";
import {NavLink} from "react-router-dom";

class Post extends Component {
    state = {
        category: Object.keys(CATEGORIES)[0],
        text :'',
        title:'',
    };
    valueChanged = event =>{
        const {name,value} = event.target;
        this.setState({[name]: value});
    };

    submitHandler=event=>{
        event.preventDefault();
        this.props.onSubmit({...this.state});
    };

    render() {
        return (
            <div className="new-post">
                {/*<Nav/>*/}
                <form className="post-form" onSubmit={this.submitHandler}>

                    <p>Category</p>
                    <select name="category" onChange={this.valueChanged} value={this.state.category}>
                        {Object.keys(CATEGORIES).map(catId =>(
                            <option key={catId} value={catId}>{CATEGORIES[catId]}</option>
                        ))}
                    </select>

                    <p>Title</p>
                    <input type="text" value={this.state.title}
                           onChange={this.valueChanged}
                           className="post-title"
                           name="author"/>

                    <p>Quote text:</p>
                    <textarea value={this.state.text}
                              onChange={this.valueChanged}
                              className="post-text" a cols="30" rows="10"
                              name="text"/>

                    <button className="add-post-btn"><NavLink to="/">Submit</NavLink></button>
                </form>
            </div>
        );
    }
}

export default Post;