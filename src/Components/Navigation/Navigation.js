import React, {Component} from 'react';
import {NavLink} from "react-router-dom";
import './Navigation.css'
class Nav extends Component {
    render() {
        return (
            <div className='nav'>
                <NavLink to="/">Home</NavLink>
                <NavLink to='/about'>About</NavLink>
                <NavLink to='/contacts'>Contacts</NavLink>
                <NavLink to='/add'>Add</NavLink>
                <NavLink to='/:id/edit'>Edit</NavLink>
                <NavLink to='/more' >More</NavLink>
                <NavLink to='/blog'>Blog</NavLink>
                <NavLink to='/products'>Our products</NavLink>
            </div>
        );
    }
}

export default Nav;