import React, {Component,Fragment} from 'react';
import Navigation from "../Components/Navigation/Navigation";
import Post from "../Components/Post";
import axios from "../axios-db";

class Add extends Component {
    addOnPage = quote =>{
        axios.post('quote.json',quote).then(()=>{
            this.props.history.replace('/')
        })
    }
    render() {
        return (
            <Fragment>
                <Navigation></Navigation>
                <h2>Add on page:</h2>
                  <Post/>
            </Fragment>
        );
    }
}

export default Add;